import React from "react"

import "../style.scss"

const IndexPage = () => (
  <section className="hero is-fullheight red-pastel">
    <div className="hero-body">
      <div className="container">
        <h1 className="is-size-1">
          <strong class="violet-pastel">Feverfew</strong>
        </h1>
        <p className="is-size-2 has-text-white">will come very soon</p>
      </div>
    </div>
  </section>
)

export default IndexPage
